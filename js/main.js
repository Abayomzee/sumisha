// JQuery ********************
$(document).ready(function () {

  // Faq
  var $faqTypeToggler = $(".faq__type");
  var $faqHeader = $(".faq__header");
  var $faqContent = $(".faq__container");
  var $faqIcon = $(".faq__icon");
  var $faqIconPlus = $(".faq__icon--plus");
  var $faqIconMinus = $(".faq__icon--minus");

  $faqHeader.click(function () {
    $(this).parent().siblings().find($faqContent).not(this).slideUp(300);
    $(this).siblings($faqContent).slideToggle(300);
  });

  // Mobile naviator
  var $openNavBtn = $(".mobile-nav__open-btn--wrapper");
  var $closeNavBtn = $(".mobile-nav__close-btn");
  var $mobilNav = $(".mobile-nav");

  $openNavBtn.click(function () {
    // $mobilNav.siblings().css({
    //   transform: "translate(-300px,200px)",
    // });
    $(".overlay").addClass("show");

    $mobilNav.css({
      transform: "translateX(-300px)",
    });
    $(this).css({
      opacity: 0,
    });
  });

  $closeNavBtn.click(function () {
    // $mobilNav.siblings().css({
    //   transform: "translate(0,0)",
    // });
    $(".overlay").removeClass("show");
    $mobilNav.css({
      transform: "translateX(0)",
    });
    $openNavBtn.css({
      opacity: 1,
    });
  });

  // Top nav dropdown
  var $dropdownWrapper = $(".dropdown");
  var $menu = $(".has-dropdown");
  var $dropdown = $(".dropdown__content");

  $menu.click(function (e) {
    e.preventDefault();
    $(this)
      .parent()
      .siblings($dropdownWrapper)
      .find($dropdown)
      .not(this)
      .removeClass("active");
    $(this).siblings($dropdown).toggleClass("active");
  });

  // Mobile nav dropdown
  var $mobileDropdownWrapper = $(".mobile-dropdown");
  var $mobileMenu = $(".mobile-dropdown__toggle");
  var $mobileDropdown = $(".mobile-dropdown__content");

  $mobileMenu.click(function (e) {
    e.preventDefault();
    $(this)
      .parent()
      .siblings($mobileDropdownWrapper)
      .find($mobileDropdown)
      .not(this)
      .slideUp();

    $(this).toggleClass("active");

    $(this).siblings($dropdown).slideToggle();
  });

});
